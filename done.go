package checkpoints

import "sync"

// DoneChan provide the channel for check the struct is done or not
type DoneChan struct {
	mu       *sync.Mutex
	doneChan chan struct{}
}

func (dc *DoneChan) getDoneChanLocked() chan struct{} {
	if dc.doneChan == nil {
		dc.doneChan = make(chan struct{})
	}
	return dc.doneChan
}

func (dc *DoneChan) getDoneChan() <-chan struct{} {
	if dc.mu == nil {
		dc.mu = new(sync.Mutex)
	}
	dc.mu.Lock()
	defer dc.mu.Unlock()
	return dc.getDoneChanLocked()
}

// Stop the channel
func (dc *DoneChan) Stop() {
	ch := dc.getDoneChanLocked()
	close(ch)
}
