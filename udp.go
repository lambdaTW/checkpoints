package checkpoints

import (
	"context"
	"fmt"
	"log"
	"net"
)

// UDPServer serve the heartbeat with UDP protocol
type UDPServer struct {
	DoneChan
	Response string
	Port     int
}

// serve the UDP connection
func (s *UDPServer) serve(
	ctx context.Context,
	conn net.PacketConn,
	addr net.Addr,
	req []byte,
) {
	log.Printf(
		"The server got connect from %s, %d bytes: %s",
		addr.String(),
		len(req),
		string(req),
	)
	conn.WriteTo([]byte(s.Response), addr)
}

// Start the UDP server
func (s *UDPServer) Start(ctx context.Context) error {
	log.Println("Launching server...")
	socket := fmt.Sprintf("0.0.0.0:%d", s.Port)
	l, err := net.ListenPacket("udp", socket)
	if err != nil {
		return err
	}
	defer l.Close()

	log.Println("Starting server...")

	for {
		buf := make([]byte, 1024)
		n, addr, err := l.ReadFrom(buf)
		if err != nil {
			select {
			case <-s.getDoneChan():
				return ErrServerClosed
			default:
			}
			continue
		}
		go s.serve(ctx, l, addr, buf[:n])
	}
	return nil
}
