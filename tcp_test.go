package checkpoints

import (
	"bufio"
	"context"
	"net"
	"strconv"
	"sync"
	"testing"
	"time"
)

const (
	resp = "RESP"
	stop = '\n'
	port = 6001
	req  = "req"
)

type connMock struct {
	*sync.Mutex
	read    string
	written string
}

type addrMock struct{}

func (addr *addrMock) Network() string {
	return ""
}

func (addr *addrMock) String() string {
	return ""
}

func (conn *connMock) SetReadDeadline(t time.Time) error {
	return nil
}

func (conn *connMock) SetWriteDeadline(t time.Time) error {
	return nil
}

func (conn *connMock) SetDeadline(t time.Time) error {
	return nil
}

func (conn *connMock) RemoteAddr() net.Addr {
	return new(addrMock)
}

func (conn *connMock) LocalAddr() net.Addr {
	return new(addrMock)
}

func (conn *connMock) Close() error {
	return nil
}

func (conn *connMock) Write(b []byte) (n int, err error) {
	conn.Lock()
	defer conn.Unlock()
	conn.written = string(b)
	return len(b), nil
}

func (conn *connMock) Read(b []byte) (n int, err error) {
	msg := req + string(stop)
	conn.read = msg
	copy(b, []byte(msg))
	return len(msg), nil
}

func newServer() *TCPServer {
	return &TCPServer{
		StopByte: stop,
		Response: resp,
		Port:     port,
	}
}
func TestTCPServer(t *testing.T) {
	t.Run("Test features", func(t *testing.T) {
		testStart(t, newServer())
		testStop(t, newServer())
		testServe(t, newServer())
	})
}

func testStart(t *testing.T, s *TCPServer) {
	ctx := context.Background()
	go s.Start(ctx)
	time.Sleep(1 * time.Millisecond)

	conn, err := net.Dial("tcp", "127.0.0.1:"+strconv.Itoa(port))
	if err != nil {
		t.Error(err)
	}
	if _, err := conn.Write([]byte(string("message\n"))); err != nil {
		t.Error(err)
	}
	for {
		if msg, err := bufio.NewReader(conn).ReadString(s.StopByte); err != nil {
			t.Error(err)
		} else {
			if len(msg) > 2 {
				if string(msg) != resp+string(stop) {
					t.Errorf("%s != %s", msg, resp)
				}
				break
			}
		}
	}
	s.Stop()
}

func testStop(t *testing.T, s *TCPServer) {
	s.Stop()
	select {
	case <-s.getDoneChan():
	case <-time.After(time.Second):
		t.Error("Time out, the server done chan return none")
	}
}

func testServe(t *testing.T, s *TCPServer) {
	conn := new(connMock)
	conn.Mutex = new(sync.Mutex)
	ctx := context.Background()
	go s.serve(ctx, conn)
	time.Sleep(100 * time.Microsecond)
	conn.Lock()
	if conn.written != (resp + string(stop)) {
		t.Error("Do not call write with the response data")
	}
	conn.Unlock()
}
