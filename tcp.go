package checkpoints

import (
	"bufio"
	"context"
	"errors"
	"log"
	"net"
	"strconv"
	"time"
)

// ErrServerClosed means: TCP server closed
var ErrServerClosed = errors.New("tcp: Server closed")

// TCPServer serve the heartbeat with TCP protocol
type TCPServer struct {
	DoneChan
	StopByte byte
	Response string
	Port     int
}

func (s *TCPServer) serve(ctx context.Context, conn net.Conn) {
	defer conn.Close()
	for {
		_, err := bufio.NewReader(conn).ReadString(s.StopByte)
		if err != nil {
			return
		}
		conn.Write([]byte(string(s.Response + string(s.StopByte))))
	}
}

// Start to listen and serve the port what given
func (s *TCPServer) Start(ctx context.Context) error {

	log.Println("Launching server...")

	// listen on all interfaces
	l, _ := net.Listen("tcp", ":"+strconv.Itoa(s.Port))
	defer l.Close()
	var tempDelay time.Duration
	for {
		rw, err := l.Accept()
		if err != nil {
			select {
			case <-s.getDoneChan():
				return ErrServerClosed
			default:
			}
			if ne, ok := err.(net.Error); ok && ne.Temporary() {
				if tempDelay == 0 {
					tempDelay = 5 * time.Millisecond
				} else {
					tempDelay *= 2
				}
				if max := 1 * time.Second; tempDelay > max {
					tempDelay = max
				}
				log.Printf("http: Accept error: %v; retrying in %v", err, tempDelay)
				time.Sleep(tempDelay)
				continue
			}
			return err
		}
		tempDelay = 0
		go s.serve(ctx, rw)
	}
}
