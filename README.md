Checkpoints
===
Checkpoints provide the server let heartbeat service can check the server status

# Package usage
## [Example](https://gitlab.com/lambdaTW/checkpoints/blob/master/cmd/)

# Command usage
## Build
```
make
```

## Run
```
# Show help
./tcp --help

# Run
./tcp -p 6001 -r ping
```

# Docker usage
## Build
```
make docker
```

## Run
```
docker run -e TCP_CHECKPOINT_PORT=6001  -e TCP_CHECKPOINT_RESPONSE=PING -p 0.0.0.0:6001:6001 checkpoints_tcp
```