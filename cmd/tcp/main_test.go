package main

import (
	"fmt"
	"os"
	"testing"
)

func Test_main(t *testing.T) {

	t.Run("Without port setting", func(t *testing.T) {
		defer func() {
			if err := recover(); err == nil {
				t.Error(err)
			}
		}()
		main()
	})

	t.Run("With port setting", func(t *testing.T) {
		os.Setenv(fmt.Sprintf("%s_%s", envPrefix, "PORT"), "6001")
		go main()
	})
}
