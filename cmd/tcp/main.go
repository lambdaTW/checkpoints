package main

import (
	"context"
	"log"
	"os"

	"gitlab.com/lambdaTW/checkpoints"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

const (
	envPrefix = "TCP_CHECKPOINT"
)

var (
	cfgFile = ""
	port    int
	stop    uint
	resp    string
)

func initConfig() {
	// Don't forget to read config either from cfgFile or from home directory!
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Search config in currently directory with name ".env" (without extension).
		viper.AddConfigPath("./")
		viper.SetConfigName("config")
	}
	// Skip the reading config error
	viper.ReadInConfig()
}

var rootCmd = &cobra.Command{
	Use:   "tcp",
	Short: "The TCP command is simple TCP checkpoint for heartbeat",
	Long: `The TCP checkpoint provide simple request/response with defined
string it can be the server live checkpoint`,
	Run: func(cmd *cobra.Command, args []string) {
		if viper.GetInt("port") == 0 {
			panic("Please provide port number for the TCP checkpoint server")
		}
		server := checkpoints.TCPServer{
			StopByte: byte(viper.GetInt("stop")),
			Response: viper.GetString("response"),
			Port:     viper.GetInt("port"),
		}
		ctx := context.Background()
		go server.Start(ctx)
		<-ctx.Done()
	},
}

func init() {
	viper.SetEnvPrefix(envPrefix)
	viper.BindEnv("config")
	viper.BindEnv("port")
	viper.BindEnv("response")

	rootCmd.PersistentFlags().UintVarP(&stop, "stop", "s", 10, "stop byte code")
	viper.BindPFlag("stop", rootCmd.PersistentFlags().Lookup("stop"))

	if viper.GetInt("port") != 0 {
		port = viper.GetInt("port")
	}
	rootCmd.PersistentFlags().IntVarP(&port, "port", "p", port, "port number would be listened")
	viper.BindPFlag("port", rootCmd.PersistentFlags().Lookup("port"))
	rootCmd.MarkFlagRequired("port")

	rootCmd.PersistentFlags().StringVarP(&resp, "response", "r", "", "response to client string")
	viper.BindPFlag("response", rootCmd.PersistentFlags().Lookup("response"))

	cfgFile = viper.GetString("config")
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", cfgFile, "config file (default is ./config.json)")
	cobra.OnInitialize(initConfig)

}

func main() {
	if err := rootCmd.Execute(); err != nil {
		log.Println(err)
		os.Exit(1)
	}
}
