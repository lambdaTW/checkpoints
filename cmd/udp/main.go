package main

import (
	"context"
	"log"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/lambdaTW/checkpoints"
)

var (
	port int
	resp string
)

const (
	envPrefix = "UDP_CHECKPOINT"
	cfgFile   = "config"
)

func initConfig() {
	// Don't forget to read config either from cfgFile or from home directory!
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Search config in currently directory with name ".env" (without extension).
		viper.AddConfigPath("./")
		viper.SetConfigName("config")
	}
	// Skip the reading config error
	viper.ReadInConfig()
}

var rootCmd = &cobra.Command{
	Use:   "udp",
	Short: "The UDP command is simple UDP checkpoint for heartbeat",
	Long: `The UDP checkpoint provide simple request/response with defined
string it can be the server live checkpoint`,
	Run: func(cmd *cobra.Command, args []string) {
		if viper.GetInt("port") == 0 {
			panic("Please provide port number for the UDP checkpoint server")
		}
		server := checkpoints.UDPServer{
			Response: viper.GetString("response"),
			Port:     viper.GetInt("port"),
		}
		ctx := context.Background()
		go server.Start(ctx)
		<-ctx.Done()
	},
}

func init() {
	viper.SetEnvPrefix(envPrefix)
	viper.BindEnv("port")
	viper.BindEnv("response")

	if viper.GetInt("port") != 0 {
		port = viper.GetInt("port")
	}
	rootCmd.PersistentFlags().IntVarP(&port, "port", "p", port, "port number would be listened")
	viper.BindPFlag("port", rootCmd.PersistentFlags().Lookup("port"))
	rootCmd.MarkFlagRequired("port")

	rootCmd.PersistentFlags().StringVarP(&resp, "response", "r", "", "response to client string")
	viper.BindPFlag("response", rootCmd.PersistentFlags().Lookup("response"))

	cobra.OnInitialize(initConfig)
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		log.Println(err)
		os.Exit(1)
	}
}
